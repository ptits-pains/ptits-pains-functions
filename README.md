# Ptits Pains Functions

## Description

This is the business part of our app. It consists of scheduling the dinner rolls days inside a team. You can find the GUI description in this link :

-[front end](https://gitlab.com/ptits-pains/ptits-pains-gui)

this part was built based on the firebase functions. We will list below the list of our functions, the triggering element of each of them and theirs roles.

### database structure

Here's our data model.

```
ptit's pains
│   currentYear                         : string
│   rollsDay                            : string
│
└───holidays                            : object
│   │
│   └───2019-01-01                      : object
│   │    │   date                       : string
│   │    │   name                       : string
│   │
│   └───2019-12-24                      : object
│       │   date                        : string
│       │   name                        : string
|
└───nonWorkingDays                      : array of strings
|      0: "0"
|      1: "6"
|
└───rollsDinner                         : array of objects
│   │
│   └───userId                          : string
│       │   nextDate                    : string
│       │   userId                      : string
|
└───rollsDinnerMonth                    : object
│   │
│   └───2019-12                         : array of objects
|       |
│       └───2019-12-13                  : object
│           │   userId                  : string
|           |
│           └───participants            : array of objects
|           |   |
│           |   └───userId              : object
│           |       │   userId          : string
|           |
│           └───products                : array of objects
│               |
│               └───0                   : object
│                   |index              : number
│                   |name               : string
│                   |quantity           : string
|
└───requests                            : object
│   │
│   └───userId                          : object
|       |
│       └───requestId                   : object
│           │   collegeDay              : string
│           │   collegeID               : string
│           │   myDay                   : string
│           │   requestTime             : string
│           │   state                   : string
|
└───notifications                       : object
│   │
│   └───userId                          : object
|       |
│       └───requestId                   : object
│           │   collegeDay              : string
│           │   collegeID               : string
│           │   myDay                   : string
│           │   requestTime             : string
│           │   state                   : string
|
└───users                               : object
│   │
│   └───userId                          : object
│       │   createdAt                   : string
│       │   deleteAccount               : boolean
│       │   emailAddress                : string
│       │   firstName                   : string
│       │   initials                    : string
│       │   isAdmin                     : boolean
│       │   lastName                    : string
│   
```

- `currentYear` : this variable will holds the current year. We gonna use this variable in the calendar settings. When the user changes the year, we will store it in this variable and all the holidays dates to match this year.


| name  | type  |  Example values |
|---|---|---|
|  currentYear | string  |  "2021" |

- `rollsDay` : this is our dinner rolls day of the week. each week, one of your colleges of the work will bring the dinner rolls it's gonna be the day held by this variable.


| name  | type  |  Example values | Description |
|---|---|---|---|
|  rollsDay | string  |  "5" | friday |

- `nonWorkingDays` : this array will hold the non working days of the week. this days will be disabled in the dinner rolls calendar.


| name  | type  |  Example values |  Description |
|---|---|---|---|
|  nonWorkingDays | array of string  |  ["0","6"] | saturday and sunday |

- `holidays` : this array will determine all holidays of the year. it can configured by and admin user from the calendar settings screen. All holidays will be disabled in the calendar.


| name  | type  |  Example values |
|---|---|---|
|  rollsDay | array of objects  |  [{2019-12-24: date: "2019-12-24", name: "christmas"}] |

- `rollsDinner` : this variable will hold all users ids of our application. for each user id, it assigns the next date he will bring the dinner rolls with him.

| name  | type  |  Example values |
|---|---|---|
|  rollsDinner | array of objects  |  [{S3INEjEZrZW2JeL9Ld8kegUrcqV2: nextDate: "2019-12-13", userId: "S3INEjEZrZW2JeL9Ld8kegUrcqV2"}] |


- `rollsDinnerMonth` : this node will hols more details about the dinner rolls day of each user. Data was divided by months in this node to make it easy to the calendar to display data. for example if we are in november 2019 and we click on the next month button from the dinner calendar, the calendar will make a call to the database to get dinner rolls for `2019-12`. It will get all the dinner rolls days of december and the data to show in each day.

Example value :
```json
{
    "2019-12": [
        {
            "2019-12-13":{
                userId: "S3INEjEZrZW2JeL9Ld8kegUrcqV2",
                participants: [
                    {
                        "kaPFatYmh3VtLcXd1ihfCj44e0T2": {
                            userId: "kaPFatYmh3VtLcXd1ihfCj44e0T2"
                        }
                    }
                ],
                products: [
                    0: {
                        index: 0,
                        name: "croissants",
                        quantity: "10"
                    }
                ]
            }
        }
    ]
}
```

* userId : is the user that will make the dinner rolls day
* participants are the users that will participate in this dinner rolls day
* products are the products that userId will brings to this day.

- `requests` : each user can ask a college to switch theirs dinner rolls days. this node will hold this request

Example value :
```json
{
    "S3INEjEZrZW2JeL9Ld8kegUrcqV2": {
        "1576156312": {
            collegeDay: "2019-12-20",
            collegeID: "kaPFatYmh3VtLcXd1ihfCj44e0T2",
            myDay: "2019-12-13",
            requestTime: "2019-12-12T13:11:54.780Z",
            state: "envoyé"
        }
    }
}
```
* S3INEjEZrZW2JeL9Ld8kegUrcqV2 is the user id. all requests of this user id will be stored under this node.
* 1576156312 is the request id. it's a unique id.
* this user (S3INEjEZrZW2JeL9Ld8kegUrcqV2) sent a request to a college (kaPFatYmh3VtLcXd1ihfCj44e0T2) to switch his dinner rolls day (2019-12-13) with his college day (2019-12-20)
* requestTime si the time when the request was sent
* state is the request state

- `notifications` : if one of the colleges asked to switch his day with the current user. this last will receive a notification and he can accept or refuse it.

Example value :
```json
{
    "kaPFatYmh3VtLcXd1ihfCj44e0T2": {
        "1576156312": {
            collegeDay: "2019-12-13",
            collegeID: "S3INEjEZrZW2JeL9Ld8kegUrcqV2",
            myDay: "2019-12-20",
            requestTime: "2019-12-12T13:11:53.973Z",
            state: "en cours"
        }
    }
}
```
* S3INEjEZrZW2JeL9Ld8kegUrcqV2 is the user id. all notifications of this user id will be stored under this node.
* 1576156312 is the request id. it's a unique id (same as the request).
* this user (kaPFatYmh3VtLcXd1ihfCj44e0T2) received a notification from (kaPFatYmh3VtLcXd1ihfCj44e0T2) to switch his dinner rolls day (2019-12-20) with his college day (2019-12-13)
* requestTime is the time when the notification was received
* state is the request state

- `users` : the users node will store some basic users profiles data such as email, user name ...

Example value :
```json
{
    "S3INEjEZrZW2JeL9Ld8kegUrcqV2": {
        "1576156312": {
            createdAt: "2019-12-12T13:11:07.690Z",
            deleteAccount: true,
            emailAddress: "amdouni@gmail.com",
            firstName: "mohamed",
            initials: "MAI",
            isAdmin: true,
            lastName: "AMDOUNI"
        }
    }
}
```
* createdAt is the data when user joined the application.
* deleteAccount will be true if the user choose to delete his account. the delete accounts function will be triggered every night to delete users that has a deleteAccount to true. 
* initials are the initials to be displayed in the calendar.
* isAdmin : if true the user will be an administrator of the application.

## Code tree

```
functions
|
│
└───auth            : will contain functions that will be triggered after an authentication action like create an account or user sign in ...
│   │
│   └───addNewUser
│       │   addNewUserToTheRollsList
|
|
└───batch           : will contain functions that will be triggered at night (in a specific hour)
│   │
│   └───deleteAccounts
│   |   │   deleteAccounts
|   |
|   |
│   └───deleteRollsLastYear
│   |   │   deleteRollsLastYear
|   |
|   |
│   └───reorderAfterDeletion
│   |   │   reorderAfterDeletion
|   |
|   |
│   └───reorderDinnerRolls
│       │   reorderDinnerRolls
|
|
└───config          : will contain the configuration files of our application
│   │
│   └───dbConfig
|
|
|
└───constants       : we will declare our constants here
│   │
│   └───state
|
|
|
└───database        : will contain functions that will be triggered after a database action action like create or update a value ...
│   │
│   └───creation
|   |   |   onUserCreate
|   |   |   onRollsDayCreation
|   |   |   onNotificationCreation
|   |   |   onRequestCreation
|   |
|   |
│   └───switchDay
│       │   onAcceptSwitchRollsDay
|
|
|
└───utils
│   │   ArrayUtils
│   │   DateUtils
│   │   ObjectUtils
│
|index              : will contains all the deployed firebase functions
|
```

| function  | description  |
|---|---|
|  addNewUserToTheRollsList | This function will be executed after each user subscription. It will will calculate the next dinner rolls day and assign it to the user  |
| deleteAccounts   | This function will be executed each day at night. It will look for users to be deleted (deleteAccount=true). If so, it will delete all user's data such as requests, notification, dinner rolls day, account ...  |
| deleteRollsLastYear | This function will be executed at 1st february of each year. It will delete the dinner rolls month nodes of the last year |
| reorderAfterDeletion | This function will be executed each day at night after the deleteAccounts function. After deleting users we gonna have empties rolls days. This function will find these days and fill them by shifting the next dinner rolls days |
|reorderDinnerRolls| This function will be executed each night. It will reorder the dinner rolls days. It will look for users that have already done their dinner rolls days, recalculate its new rolls days and finally add them to the end of the list|
|onUserCreate| This function will be executed after a user creation to add the time at which this user has been created|
|onRollsDayCreation| This function will be executed after the first user of the configures the dinner rolls day of the week. It will take all exist users, calculate the next rolls day for each one and finally add them to the list|
|onNotificationCreation| This function will be executed after a notification creation to add the time at which this notification has been created|
|onRequestCreation| This function will be executed after a request creation to add the time at which this request has been created|
| onAcceptSwitchRollsDay | This function will be triggered when a user accepts to switch his dinner rolls day with a college. As it's name says, it will switch the two users days data |

## Requirements

For development, you will only need the Node.js and firebase cli installed on your environment. 

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.16.3

    $ npm --version
    6.9.0

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

### Firebase CLI

You can download and install the cli from :

- [firebase cli](https://firebase.google.com/docs/cli)

To create a new firebase project (if you didn't do it yet) check this :

- [https://firebase.google.com/docs/functions/get-started](https://firebase.google.com/docs/functions/get-started)

Make sure you are running the latest version of firebase tools. You can do that by re-installing firebase tools.

    $ npm install -g firebase-tools

## Installation

    $ git clone https://gitlab.com/ptits-pains/ptits-pains-functions.git
    $ cd ptits-pains-functions

### Configure the app

Login to your firebase account :

    $ firebase login

choose your project :

    $ firebase use --add

```
? Which project do you want to add? ptits-pains
```

Choose the default alias :

```
? What alias do you want to use for this project? (e.g. staging) default
```

to install eslint and other dependencies :

    $ cd functions && npm install

to deploy functions :

    $ firebase deploy --only functions

If you have this error :

**Error: HTTP Error: 400, Billing must be enabled for activation of service '[cloudscheduler.googleapis.com]' in project '995434042985' to proceed.**

this link will help you :

- [enable billing](https://cloud.google.com/billing/docs/how-to/modify-project?hl=en)
