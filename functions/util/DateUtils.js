const moment = require("moment");

/**
 * return number of days between two dates (signed return). dates form is string YYYY-MM-DD
 */
const realDiffTwoDatesDaysString = (date1, date2) => {
  return stringToMomentDate(date1).diff(stringToMomentDate(date2), "days");
};

/**
 * format and return a string as a date
 * @param {string} str
 */
const stringToMomentDate = str => {
  return moment(str, "YYYY-MM-DD");
};

/**
 * format and return a string month as a date
 * @param {string} str
 */
const stringMonthToMomentDate = str => {
  return moment(str, "YYYY-MM");
};

/**
 * returns true if date1 < date2
 * @param {Moment} date1 date 1
 * @param {Moment} date2 date 2
 */
const isBefore = (date1, date2) => {
  if (!date1 || !date2) return false;
  return date1.isBefore(date2, "day");
};

/**
 * format and return a date as a string
 * @param {moment} date
 */
const momentDateToString = date => {
  return moment(date).format("YYYY-MM-DD");
};

/**
 * calculate next time this user will bring the dinners rolls
 */
const calculateNextDate = (date, rollsDay) => {
  let nextDate = moment().day(rollsDay);
  if (date) {
    date = stringToMomentDate(date);
    nextDate = date.add(1, "weeks").day(rollsDay);
  }

  if (isBefore(nextDate, now())) {
    nextDate = nextDate.add(1, "weeks");
  }
  return momentDateToString(nextDate);
};

/**
 * return time
 */
const now = () => {
  return moment();
};

module.exports = {
  realDiffTwoDatesDaysString: realDiffTwoDatesDaysString,
  stringToMomentDate: stringToMomentDate,
  isBefore: isBefore,
  now: now,
  momentDateToString: momentDateToString,
  calculateNextDate: calculateNextDate,
  stringMonthToMomentDate: stringMonthToMomentDate
};
