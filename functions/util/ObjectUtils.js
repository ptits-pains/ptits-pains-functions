
/**
 * this will be used to clone a js object
 * @param {Object} src source object
 */
function clone(src) {
    return JSON.parse(JSON.stringify(src));
}

module.exports = {
    clone: clone
};