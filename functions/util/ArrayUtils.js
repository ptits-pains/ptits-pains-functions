/**
 * remove en element from an array
 * @param {primitive Object} element element to remove
 * @param {Array} array js Array
 */
const removeElement = (element, array) => {
  if (!array || array.size === 0) return;
  let index = array.indexOf(element);
  if (index !== -1) {
    array.splice(index, 1);
  }
};

/**
 * returns true if the the array contains the element false otherwise
 * @param {Array} array a js array
 * @param {primitive} element element to find
 */
const contains = (array, element) => {
  if (!array || array.size === 0) return false;
  if (array.indexOf(element) !== -1) {
    return true;
  }
  return false;
};

/**
 * get last not null value
 * @param {Array} array a js array
 */
const getLastNotNullNextDate = array => {
  if (!array || array.length <= 0) return null;

  for (let i = array.length - 1; i >= 0; i--) {
    if (array[i].nextDate) return array[i].nextDate;
  }

  return null;
};

module.exports = {
  removeElement: removeElement,
  contains: contains,
  getLastNotNullNextDate: getLastNotNullNextDate
};
