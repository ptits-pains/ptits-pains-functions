const functions = require("firebase-functions"),
  admin = require("firebase-admin");

/*
 *   in this file we will initialize our firebase admin db
 */

admin.initializeApp(functions.config().firebase);

module.exports = admin.database().ref();
