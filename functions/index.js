const creation = require("./database/creation"),
  switchDay = require("./database/switchDay"),
  reorder = require("./batch/reorderDinnerRolls"),
  deleteAccounts = require("./batch/deleteAccounts"),
  reorderAfterDeletion = require("./batch/reorderAfterDeletion"),
  deleteRolls = require("./batch/deleteRollsLastYear"),
  addNewUser = require("./auth/addNewUser");

// database functions
exports.onUserCreate = creation.onUserCreate;
exports.onNotificationCreation = creation.onNotificationCreation;
exports.onRequestCreation = creation.onRequestCreation;
exports.onRollsDayCreation = creation.onRollsDayCreation;
exports.onAcceptSwitchRollsDay = switchDay.onAcceptSwitchRollsDay;

// batch functions
exports.reorderDinnerRolls = reorder.reorderDinnerRolls;
exports.deleteAccounts = deleteAccounts.deleteAccounts;
exports.reorderAfterDeletion = reorderAfterDeletion.reorderAfterDeletion;
exports.deleteRollsLastYear = deleteRolls.deleteRollsLastYear;

// authentication functions
exports.addNewUserToTheRollsList = addNewUser.addNewUserToTheRollsList;
