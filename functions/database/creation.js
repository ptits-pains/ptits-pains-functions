const functions = require('firebase-functions'),
    dateUtils = require("../util/DateUtils"),
    dbRef = require("../config/dbConfig");

/*
*   in this file we will declare all our database creation triggers functions
*/

// after creating each user we will add the creation time before that we will add this user to rolls dinner list
const handleUserCreation = functions
    .region("europe-west2")
    .database
    .ref('/users/{userId}')
    .onCreate((snapshot, context) => {

        const userId = context.params.userId;
        console.log(`A new user has been created _ ${userId} on ${Date.now()}`);

        return snapshot.ref.update({ createdAt: new Date().toISOString() })
    })

// this trigger will be fired after a notification creation to add the notification create time
const handleNotificationCreation = functions
    .region("europe-west2")
    .database.ref("/notifications/{userId}/{notificationId}")
    .onCreate((snapshot, context) => {
        return snapshot.ref.update({ requestTime: new Date().toISOString() });
    });

// this trigger will be fired after a request creation to add the request create time
const handleRequestCreation = functions
    .region("europe-west2")
    .database.ref("/requests/{userId}/{requestId}")
    .onCreate((snapshot, context) => {
        return snapshot.ref.update({ requestTime: new Date().toISOString() });
    });

// after configuring the app rolls day, we gonna calculate and add the next dinner rolls days
const handleRollsDayCreation = functions
    .region("europe-west2")
    .database
    .ref('/rollsDay')
    .onCreate((snapshot, context) => {

        Promise.all([
            dbRef.child("users").once("value"),
            dbRef.child("holidays").once("value")
        ])
            .then(values => {

                let users = values[0].val();
                let holidays = values[1].val();
                let rollsDay = snapshot.val();

                console.log('users of the app', users);
                console.log('holidays of the app', holidays);
                console.log('rollsDay of the app', rollsDay);

                if (!users || !rollsDay) {
                    console.log('there is no users or the rolls day is not defined');
                    return null;
                }

                let usersRollsDays = getNextDinnerRollsDays(users, holidays, rollsDay)

                console.log('usersRollsDays', usersRollsDays)
                if (usersRollsDays.length === 0) return null;

                let usersRollsDaysToStore = [];
                usersRollsDays.forEach(user => {

                    let monthLabel = user.nextDate.substring(0, 7);
                    usersRollsDaysToStore.push(
                        dbRef.child("rollsDinner").child(user.userId).set({ userId: user.userId, nextDate: user.nextDate }),
                        dbRef.child("rollsDinnerMonth").child(monthLabel).child(user.nextDate).set({ userId: user.userId })
                    )
                })
                return Promise.all(usersRollsDaysToStore);
            })
            .then(values => {
                console.log('the process of adding records after the rolls day creation was successfully terminated');
                return null;
            })
            .catch((err) => {
                console.log(err);
            })
    })

module.exports = {
    onUserCreate: handleUserCreation,
    onRollsDayCreation: handleRollsDayCreation,
    onNotificationCreation: handleNotificationCreation,
    onRequestCreation: handleRequestCreation
};

function getNextDinnerRollsDays(users, holidaysRes, rollsDay) {

    let lastDinnerRollsDay = null;
    let holidays = holidaysRes ? Object.keys(holidaysRes) : null;

    let response = [];
    Object.keys(users).forEach(key => {

        let nextDate = dateUtils.calculateNextDate(lastDinnerRollsDay, rollsDay);
        while (holidays && holidays.includes(nextDate)) {

            console.log(`the calculated nex date is ${nextDate} but unfortunately it's a holiday`);
            nextDate = dateUtils.calculateNextDate(nextDate, rollsDay);
        }
        console.log(`The next date for ${key} will be ${nextDate}`);
        response.push(
            {
                userId: key,
                nextDate: nextDate
            }
        )
        lastDinnerRollsDay = nextDate;
    });
    return response;
}