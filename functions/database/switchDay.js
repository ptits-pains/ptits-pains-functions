const functions = require("firebase-functions"),
  stateConstants = require("../constants/state"),
  dbRef = require("../config/dbConfig");

/*
 *   in this file we will declare the switch dinner rolls day process
 */

// this trigger will be fired each time the user accepts to change his rolls day with a college
const onAcceptSwitchRollsDay = functions
  .region("europe-west2")
  .database.ref("/notifications/{userId}/{notificationId}")
  .onUpdate((change, context) => {

    const myId = context.params.userId;
    const notificationId = context.params.notificationId;

    const after = change.after.val();
    const before = change.before.val();

    console.log("after", after);
    console.log("after.state", after.state);

    if (before.state === after.state) {
      console.log("state didn't change");
      return null;
    }

    // if the request was accepted we will switch the rolls days of the users
    if (after.state === stateConstants.ACCEPTED) {

      console.log(`the switch order number ${notificationId} was accepted`);
      const { collegeDay, collegeID, myDay } = after;

      switchRollsData(collegeDay, myDay, collegeID, myId)
        .then(() => {

          // the user day is different now so we gonna disable all his other notifications
          return dbRef.child(`notifications/${myId}`).once("value");
        })
        .then((response) => {

          let notifications = response.val();
          console.log('the user notifications are : ', notifications)

          let toUpdateNotifications = [];
          Object.keys(notifications).forEach(notification => {
            if (notification !== notificationId && notifications[notification].state === stateConstants.IN_PROGRESS) {
              toUpdateNotifications.push(
                dbRef.child(`notifications/${myId}/${notification}`).update({ state: stateConstants.DISABLED }),
                // we gonna disable the request assigned to this notification
                dbRef.child(`requests/${notifications[notification].collegeID}/${notification}`).update({ state: stateConstants.DISABLED }),
              )
            }
          });

          if (toUpdateNotifications.length > 0) {
            return Promise.all(toUpdateNotifications);
          } else {
            return null;
          }
        })
        .then(() => {

          // well the user date has changed so we gonna disable his requests
          return dbRef.child(`requests/${myId}`).once("value");
        })
        .then((response) => {

          let userRequests = response.val();
          let toUpdateRequests = [];
          Object.keys(userRequests).forEach(requestId => {
            if (userRequests[requestId].state === stateConstants.SENT) {
              toUpdateRequests.push(
                dbRef.child(`requests/${myId}/${requestId}`).update({ state: stateConstants.DISABLED }),
                //we gonna also updated the notifications assigned to this request
                dbRef.child(`notifications/${userRequests[requestId].collegeID}/${requestId}`).update({ state: stateConstants.DISABLED })
              )
            }
          })
          if (toUpdateRequests.length > 0) {
            return Promise.all(toUpdateRequests);
          } else {
            return null;
          }
        })
        .then(() => {

          // the college day is different now so we gonna disable all his other requests
          return dbRef.child(`requests/${collegeID}`).once("value");
        })
        .then((response) => {

          let requests = response.val();
          console.log('the college requests are : ', requests)

          let toUpdateRequests = [];
          Object.keys(requests).forEach(requestId => {
            if (requestId !== notificationId && requests[requestId].state === stateConstants.SENT) {
              requests[requestId].state = stateConstants.DISABLED;
              toUpdateRequests.push(
                dbRef.child(`requests/${collegeID}/${requestId}`).update({ state: stateConstants.DISABLED }),
                //we gonna also updated the sent notifications
                dbRef.child(`notifications/${requests[requestId].collegeID}/${requestId}`).update({ state: stateConstants.DISABLED })
              )
            }
          });

          if (toUpdateRequests.length > 0) {
            return Promise.all(toUpdateRequests);
          } else {
            return null;
          }
        })
        .then(() => {

          // the same process for the college notifications
          return dbRef.child(`notifications/${collegeID}`).once("value");
        })
        .then((response) => {

          let notifications = response.val();
          console.log('the college notifications are : ', notifications)

          let toUpdateNotifications = [];
          Object.keys(notifications).forEach(notification => {
            if (notifications[notification].state === stateConstants.IN_PROGRESS) {
              toUpdateNotifications.push(
                dbRef.child(`notifications/${collegeID}/${notification}`).update({ state: stateConstants.DISABLED }),
                // we gonna disable the request assigned to this notification
                dbRef.child(`requests/${notifications[notification].collegeID}/${notification}`).update({ state: stateConstants.DISABLED }),
              )
            }
          });

          if (toUpdateNotifications.length > 0) {
            return Promise.all(toUpdateNotifications);
          } else {
            return null;
          }
        })
        .then(() => {

          console.log('the switch process was terminated successfully');
          return change.after.ref.updated({
            stateEditTime: new Date().toISOString()
          });
        })
        .catch(error => {
          console.log(error)
        });
    } else {

      return change.after.ref.updated({
        stateEditTime: new Date().toISOString()
      });
    }
  });


function switchRollsData(collegeDay, myDay, collegeID, myId) {

  let collegeMonthNode = collegeDay.substring(0, 7);
  let myMonthNode = myDay.substring(0, 7);
  return Promise.all([
    dbRef.child(`rollsDinnerMonth/${collegeMonthNode}/${collegeDay}`).once("value"),
    dbRef.child(`rollsDinnerMonth/${myMonthNode}/${myDay}`).once("value")
  ])
    .then(values => {

      let collegeRollsData = values[0].val();
      let myRollsData = values[1].val();

      console.log('collegeRollsData', collegeRollsData);
      console.log('myRollsData', myRollsData);

      return Promise.all([
        //switch users data
        dbRef.child(`rollsDinnerMonth/${collegeMonthNode}/${collegeDay}`).set(myRollsData),
        dbRef.child(`rollsDinnerMonth/${myMonthNode}/${myDay}`).set(collegeRollsData),

        //switch users rolls dates
        dbRef.child(`rollsDinner/${collegeID}`).update({ nextDate: myDay }),
        dbRef.child(`rollsDinner/${myId}`).update({ nextDate: collegeDay })
      ])
    })
}

module.exports = {
  onAcceptSwitchRollsDay: onAcceptSwitchRollsDay
};
