const admin = require("firebase-admin"),
    functions = require("firebase-functions"),
    dbRef = require("../config/dbConfig");

/*
*  source : https://firebase.google.com/docs/auth/admin/manage-users
*  this batch will delete users accounts (that must be deleted)
*/

const deleteAccounts = functions
    .region("europe-west2")
    .pubsub.schedule("every day 01:00")
    .timeZone("Europe/Paris")
    .onRun(context => {

        dbRef.child("users").once("value")
            .then((snap) => {

                let users = snap.val();
                let toDeleteUsers = [];
                if (users) {
                    Object.keys(users).forEach(key => {
                        if (users[key].deleteAccount === true) {
                            toDeleteUsers.push(key)
                        }
                    });
                }

                console.log('users to be deleted', toDeleteUsers)

                if (toDeleteUsers.length > 0) {

                    rescheduleRollsDays = true;
                    toDeleteUsers.forEach(uid => {
                        deleteUser(uid);
                        clearUserData(uid);
                    })
                }

                return null;

            }).catch(error => {
                console.log(error);
            });
    });

function deleteUser(uid) {
    admin.auth().deleteUser(uid)
        .then(() => {
            console.log('Successfully deleted user');
            return null;
        })
        .catch((error) => {
            console.log('Error deleting user : ', uid, error);
        });
}

function clearUserData(uid) {

    //delete notifications
    clearNotifications(uid);

    //delete requests sent by other users to this user (uid)
    clearRequests(uid);

    //delete rolls dinner data
    clearRolls(uid);

    //delete user data
    dbRef.child("users").child(uid).remove();
}

function clearRequests(uid) {
    //delete requests made by this user to others
    dbRef.child("requests").child(uid).once("value")
        .then((snap) => {

            let requests = snap.val();
            console.log('requests', requests)
            let nodesToDelete = [];
            if (requests) {

                Object.keys(requests).forEach(requestId => {

                    let collegeID = requests[requestId].collegeID;
                    nodesToDelete.push(
                        dbRef.child("notifications").child(collegeID).child(requestId).remove()
                    )
                })

                return Promise.all(nodesToDelete);
            }

            return null;
        }).then(() => {

            //delete requests
            dbRef.child("requests").child(uid).remove();
            return null;
        })
        .catch((error) => {
            console.log('Error occurred while process requests data', uid, error);
        });
}

function clearRolls(uid) {

    dbRef.child("rollsDinner").child(uid).once("value")
        .then((snap) => {

            let rollsData = snap.val();

            //we will get the next rolls day and delete it
            let rollsNextDate = rollsData.nextDate;
            console.log('deleting the next rolls date', rollsNextDate);
            return dbRef.child("rollsDinnerMonth")
                .child(rollsNextDate.substring(0, 7))
                .child(rollsNextDate)
                .remove();
        }).then(() => {

            dbRef.child("rollsDinner").child(uid).remove();
            return null;
        })
        .catch((error) => {
            console.log('Error occurred while retrieving rolls data', uid, error);
        });
}

function clearNotifications(uid) {

    //delete requests made by others to this user (uid)
    dbRef.child("notifications").child(uid).once("value")
        .then((snap) => {

            let notifications = snap.val();
            console.log('notifications', notifications)
            let nodesToDelete = [];
            if (notifications) {

                Object.keys(notifications).forEach(notificationId => {

                    let collegeID = notifications[notificationId].collegeID;
                    nodesToDelete.push(
                        dbRef.child("requests").child(collegeID).child(notificationId).remove()
                    )
                })

                return Promise.all(nodesToDelete);
            }

            return null;
        }).then(() => {

            //delete notifications
            dbRef.child("notifications").child(uid).remove();
            return null;
        })
        .catch((error) => {
            console.log('Error occurred while process notifications data', uid, error);
        });
}

module.exports = {
    deleteAccounts: deleteAccounts
};    