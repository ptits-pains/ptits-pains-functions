const functions = require("firebase-functions"),
    dbRef = require("../config/dbConfig"),
    objectUtils = require("../util/ObjectUtils");

/*
*  this batch will be used to delete the rolls month of the last year
*/

const deleteRollsLastYear = functions
    .region("europe-west2")
    .pubsub.schedule("1st monday of february 05:00")
    .timeZone("Europe/Paris")
    .onRun(context => {

        dbRef.child("rollsDinnerMonth").once("value")
            .then((snap) => {

                let rollsDinnerMonth = snap.val();
                console.log('current rolls dinner month node value is ', objectUtils.jsonCopy(rollsDinnerMonth))

                if (!rollsDinnerMonth) return null;

                let currentYear = new Date().getFullYear();

                Object.keys(rollsDinnerMonth).forEach(key => {
                    if (parseInt(key.substring(0, 4)) < currentYear) {
                        delete rollsDinnerMonth[key]
                    }
                })

                console.log('current rolls dinner month node value is ', objectUtils.jsonCopy(rollsDinnerMonth))
                return dbRef.child("rollsDinnerMonth").set(rollsDinnerMonth)

            })
            .then(() => {

                console.log('the deleting of the dinner rolls per month of the last year was terminated successfully')
                return null;
            })
            .catch(error => {
                console.log(error);
            });
    });


module.exports = {
    deleteRollsLastYear: deleteRollsLastYear
};    