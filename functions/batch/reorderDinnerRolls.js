const functions = require("firebase-functions"),
  dbRef = require("../config/dbConfig"),
  dateUtils = require("../util/DateUtils"),
  arrayUtils = require("../util/ArrayUtils"),
  objectUtils = require("../util/ObjectUtils");

/**
 *  this function will be triggered each week to reorder the existing dinner rolls configuration
 *  for example if hugo did his dinner rolls day today, this function will be run in end of the week (as we have a single dinner rolls
 *  day per week) to calculate his next dinner rolls day and add him to the end of the list
 */

const reorderDinnerRolls = functions
  .region("europe-west2")
  .pubsub.schedule("every sunday 04:00")
  .timeZone("Europe/Paris")
  .onRun(context => {
    Promise.all([
      dbRef.child("rollsDinner").once("value"),
      dbRef.child("rollsDay").once("value"),
      dbRef.child("holidays").once("value")
    ])
      .then(values => {

        // ordering the rolls dinner node
        let rollsDinnerResponse = values[0].val();
        let rollsDay = values[1].val();
        let holidays = values[2].val();

        console.log('rollsDinnerResponse', rollsDinnerResponse);
        console.log('rollsDay', rollsDay);
        console.log('holidays', holidays);

        if (!rollsDay) return null;

        let rollsDinnerArray = sortByDateRollsDinner(rollsDinnerResponse);

        // now we will schedule the rolls days
        let result = reScheduleRollsDays(rollsDinnerArray, rollsDay, holidays);

        if (result) {

          return Promise.all(result)
        } else {

          return null;
        }
      })
      .then(values => {

        console.log('finish ordering the rolls dinner node');
        console.log('values', values);

        if (!values) {

          console.log('there is nothing to order');
          return null;
        } else {

          return Promise.all([
            dbRef.child("rollsDinner").once("value"),
            dbRef.child("rollsDinnerMonth").once("value")
          ])
        }
      })
      .then(values => {

        if (!values) return null;

        // now we gonna order the rollsDinnerMonth node
        let rollsDinner = values[0].val();
        let rollsDinnerMonth = values[1].val();

        console.log('current rolls dinner node value is ', objectUtils.clone(rollsDinner));
        console.log('rolls dinner month to update is ', objectUtils.clone(rollsDinnerMonth));

        if (!rollsDinner || rollsDinner.length === 0) return null;

        rollsDinner = sortByDateRollsDinner(rollsDinner);

        rollsDinner.forEach(user => {

          let monthLabel = user.nextDate.substring(0, 7);
          if (rollsDinnerMonth[monthLabel]) {

            // if the month label exists
            rollsDinnerMonth[monthLabel][user.nextDate] = {
              userId: user.userId
            }
          } else {

            //otherwise we will add the month node and the user
            rollsDinnerMonth[monthLabel] = {
              [user.nextDate]: {
                userId: user.userId
              }
            }
          }
        });

        console.log('rolls dinner month after updating will be ', rollsDinnerMonth);
        return rollsDinnerMonth;
      })
      .then(rollsDinnerMonth => {

        if (!rollsDinnerMonth) {

          console.log('there is nothing to update');
          return null;
        } else {

          return dbRef.child("rollsDinnerMonth").set(rollsDinnerMonth)
        }
      })
      .then(() => {

        console.log('finish updating the rolls dinner month node');
        return null;
      })
      .catch(error => {
        logError(error);
      });
  });

/**
 * log the error
 */
logError = error => {
  console.log("error ocurred while ordering dinner rolls.", error);
};

/**
 * sort by date the dinner rolls days
 * @param {Array} rollsDinner the dinner rolls days per user
 */
function sortByDateRollsDinner(rollsDinnerResponse) {

  if (!rollsDinnerResponse) return null;

  let rollsDinnerArray = [];
  Object.keys(rollsDinnerResponse).forEach(key => {
    const rollsDinner = rollsDinnerResponse[key];
    rollsDinnerArray.push(rollsDinner);
  });

  // sort the rolls dinners by date
  rollsDinnerArray.sort((a, b) => {
    return dateUtils.realDiffTwoDatesDaysString(a.nextDate, b.nextDate);
  });

  console.log("rolls dinner after sorting", rollsDinnerArray);
  return rollsDinnerArray;
}

/**
 * reschedule the rolls days
 * @param {Array} array old rolls days
 */
function reScheduleRollsDays(rollsDinnerArray, rollsDay, holidaysObj) {

  let outDatedRollsDays = getOutDatedDinnerRollsDays(rollsDinnerArray);
  console.log("outDatedArray", outDatedRollsDays);
  if (outDatedRollsDays.length === 0) return null;


  let lastDinnerRollsDay = arrayUtils.getLastNotNullNextDate(rollsDinnerArray);
  let holidays = holidaysObj ? Object.keys(holidaysObj) : null;

  let response = [];
  outDatedRollsDays.forEach(user => {

    let nextDate = dateUtils.calculateNextDate(lastDinnerRollsDay, rollsDay);
    while (holidays && arrayUtils.contains(holidays, nextDate)) {

      console.log(`the calculated nex date is ${nextDate} but unfortunately it's a holiday`);
      nextDate = dateUtils.calculateNextDate(nextDate, rollsDay);
    }
    console.log(`The next date for ${user.userId} will be ${nextDate}`);
    response.push(
      dbRef
        .child("rollsDinner")
        .child(user.userId)
        .update({
          nextDate: nextDate
        })
    )
    lastDinnerRollsDay = nextDate;
  });
  return response;
}

/**
 * in this function we will determine users that have brought already the dinner rolls
 * rollsDinnerArray has this format [{userId: "XXXXXX", nextDate:"XXXX-XX-XX"}, ...]
 * @param {Array} rollsDinnerArray all dinner rolls days
 */
function getOutDatedDinnerRollsDays(rollsDinnerArray) {

  let outDatedArray = [];
  rollsDinnerArray.forEach(user => {

    if (!user.nextDate) outDatedArray.push(user);
    else if (dateUtils.isBefore(dateUtils.stringToMomentDate(user.nextDate), dateUtils.now())) {
      outDatedArray.push(user);
    }
  });
  return outDatedArray;
}

module.exports = {
  reorderDinnerRolls: reorderDinnerRolls
};
