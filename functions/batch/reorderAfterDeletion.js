const dbRef = require("../config/dbConfig"),
    dateUtils = require("../util/DateUtils"),
    functions = require("firebase-functions"),
    moment = require("moment");

// this is not a firebase trigger it's just an utility function which gonna be called after the delete accounts trigger
// this function will be used to reschedule the rolls days after some rolls days delete (if a user was deleted, its rolls days gonna be deleted)

const reorderAfterDeletion = functions
    .region("europe-west2")
    .pubsub.schedule("every day 01:30")
    .timeZone("Europe/Paris")
    .onRun(context => {

        Promise.all([

            //get all rolls days
            dbRef.child("rollsDinner").once("value"),

            //get rollsDay
            dbRef.child("rollsDay").once("value"),

            //get users next rolls day data
            dbRef.child("rollsDinnerMonth").once("value")
        ])
            .then(values => {
                let rollsDinnerDays = values[0].val();
                let rollsDay = values[1].val();
                let rollsDaysData = values[2].val();

                console.log('rollsDinnerDays', rollsDinnerDays);
                console.log('rollsDay', rollsDay);
                console.log('rollsDaysData', rollsDaysData);

                if (!rollsDay) return null;

                //now for each user object from rollsDinnerDays we gonna add its next rolls days data
                let usersData = assignUserRollsData(rollsDinnerDays, rollsDaysData);

                // sort the usersData by date
                usersData.sort((a, b) => {
                    return dateUtils.realDiffTwoDatesDaysString(a.nextDate, b.nextDate);
                });

                // now we will schedule the rolls days
                usersData = reScheduleRollsDays(usersData, rollsDay);

                // after scheduling data we will format the data to store it
                let rollsDinner = formatRollsDinner(usersData);
                let rollsDinnerMonth = formatRollsDinnerMonth(usersData);

                //store data
                return Promise.all([

                    dbRef.child("rollsDinner").set(rollsDinner),
                    dbRef.child("rollsDinnerMonth").set(rollsDinnerMonth)
                ])
            })
            .then(() => {

                console.log('the rescheduling of the rolls days after the accounts deletion was terminated successfully')
                return null;
            })
            .catch(error => {
                console.log(error);
            });
    });

/**
 * retrieve the next date and user id from users data and render it as an array
 * @param {Array} usersData users data array
 */
function formatRollsDinner(usersData) {

    let response = {};
    usersData.forEach(userData => {

        response[userData.userId] = {
            userId: userData.userId,
            nextDate: userData.nextDate
        }
    })

    console.log('formatRollsDinner', response)
    return response;
}

/**
 * retrieve the users rolls days data (participants, products ...) and render it as an array
 * @param {Array} usersData users data array
 */
function formatRollsDinnerMonth(usersData) {

    let response = {};
    usersData.forEach(userData => {

        let nextDate = userData.nextDate;
        let monthLabel = nextDate.substring(0, 7);

        if (response[monthLabel]) {
            // if the month node exists we will add the day
            response[monthLabel][userData.nextDate] = {
                userId: userData.userId,
                participants: userData.participants ? userData.participants : null,
                products: userData.products ? userData.products : null
            }
        } else {
            //otherwise we will add the month node and the user
            response[monthLabel] = {
                [userData.nextDate]: {
                    userId: userData.userId,
                    participants: userData.participants ? userData.participants : null,
                    products: userData.products ? userData.products : null
                }
            }
        }
    })

    console.log('formatRollsDinnerMonth', response)
    return response;
}

/**
 * reschedule the rolls days
 * @param {Array} usersData the users rolls days to reschedule
 */
function reScheduleRollsDays(usersData, rollsDay) {

    let nextRollsDay = moment().day(rollsDay);
    let nextRollsDays = [];
    for (let i = 0; i < usersData.length; i++) {
        nextRollsDays.push(
            nextRollsDay
        );
        nextRollsDay = nextRollsDay.clone().add(1, "weeks").day(rollsDay);
    }

    console.log('nextRollsDays', nextRollsDays);

    usersData.forEach((userData, index) => userData.nextDate = dateUtils.momentDateToString(nextRollsDays[index]));

    console.log('after setting new dates', usersData);

    return usersData;
}

// search for the user rolls day from rollsDaysData and assign its data to the rollsDinnerDays
function assignUserRollsData(rollsDinnerDays, rollsDaysData) {

    function searchForUserData(nextDate) {

        let monthLabel = nextDate.substring(0, 7);
        console.log('monthLabel', monthLabel);
        let response = rollsDaysData[monthLabel][nextDate];
        console.log('found user roll day', response);
        return response;
    }

    let responseArray = [];
    Object.keys(rollsDinnerDays).forEach(userId => {
        let nextDate = rollsDinnerDays[userId].nextDate;
        let userData = {};
        if (nextDate) {
            userData = searchForUserData(nextDate);
            userData.nextDate = nextDate;
        } else {
            userData.userId = userId;
        }
        responseArray.push(userData);
    });

    console.log('responseArray', responseArray)
    return responseArray;
}

module.exports = {
    reorderAfterDeletion: reorderAfterDeletion
}