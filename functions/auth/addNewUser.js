const functions = require("firebase-functions"),
  dbRef = require("../config/dbConfig"),
  arrayUtils = require("../util/ArrayUtils"),
  dateUtils = require("../util/DateUtils");

/*
 *   in this file we will declare a function that will add a new user to the dinner rolls list
 */

const addNewUserToTheRollsList = functions
  .region("europe-west2")
  .auth.user()
  .onCreate(user => {
    console.log("Adding a new user", user);
    let userId = user.uid;

    Promise.all([
      dbRef.child("rollsDinner").once("value"),
      dbRef.child("rollsDay").once("value"),
      dbRef
        .child("rollsDinner")
        .child(userId)
        .once("value")
    ])
      .then(values => {
        let rollsDinnerResponse = values[0].val();
        let rollsDay = values[1].val();
        let userExist = values[2].val();

        if (rollsDay) {
          //this trigger will be called more than once (see firebase documentation) so we will check
          //if this user has already a rolls day otherwise we will add it to the list
          if (userExist && userExist.userId) {
            console.log("user has already a roll day", userId);
          } else {
            console.log(
              "this user does not have a roll day. it's a new user",
              userId
            );
            if (
              rollsDinnerResponse &&
              Object.keys(rollsDinnerResponse).length > 0
            ) {
              let responseArray = [];
              Object.keys(rollsDinnerResponse).forEach(key => {
                const rollsDinner = rollsDinnerResponse[key];
                responseArray.push(rollsDinner);
              });

              // sort the rolls dinners by date
              responseArray.sort((a, b) => {
                return dateUtils.realDiffTwoDatesDaysString(
                  a.nextDate,
                  b.nextDate
                );
              });

              console.log("response array after sorting it", responseArray);
              let lastDay = arrayUtils.getLastNotNullNextDate(responseArray);
              console.log("the last day of the list is : ", lastDay);

              let userRollsDay = dateUtils.calculateNextDate(lastDay, rollsDay);
              console.log(
                `the user email : ${user.email} , the user rolls day : ${userRollsDay}`
              );

              //we will add this user to the rolls dinner list
              storeUserRollsDay(userId, user, userRollsDay);
            } else {
              let userRollsDay = dateUtils.calculateNextDate(null, rollsDay);
              console.log(
                `this is our first user email : ${user.email} , the user rolls day : ${userRollsDay}`
              );
              //this is our first rolls day
              storeUserRollsDay(userId, user, userRollsDay);
            }
          }
        }

        return null;
      })
      .catch(error => {
        console.log(error);
      });

    return null;
  });

function storeUserRollsDay(userId, user, userRollsDay) {
  // add the user to the rollsDinner list
  dbRef
    .child("rollsDinner")
    .child(userId)
    .set({
      userId: user.uid,
      nextDate: userRollsDay
    });

  //init the rolls of the month with the user data
  let monthNode = userRollsDay.substring(0, 7);
  dbRef
    .child("rollsDinnerMonth")
    .child(monthNode)
    .child(userRollsDay)
    .set({
      userId: user.uid
    });
}

module.exports = {
  addNewUserToTheRollsList: addNewUserToTheRollsList
};
